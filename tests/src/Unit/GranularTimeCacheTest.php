<?php

declare(strict_types=1);
namespace Drupal\Tests\granulartimecache\Unit;

use Drupal\Core\Cache\NullBackend;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\KeyValueStore\KeyValueMemoryFactory;
use Drupal\Core\Lock\NullLockBackend;
use Drupal\Core\State\State;
use Drupal\granulartimecache\GranularTimeCacheService;
use Drupal\Tests\granulartimecache\Unit\Double\CacheTagsInvalidatorSpy;
use Drupal\Tests\granulartimecache\Unit\Double\FakeTime;
use Drupal\Tests\granulartimecache\Unit\Double\FakeTimeZone;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

final class GranularTimeCacheTest extends TestCase {

  use ProphecyTrait;

  protected string $moduleRoot;

  protected FakeTime $time;

  protected FakeTimeZone $timeZone;
  protected CacheTagsInvalidatorSpy $cacheTagsInvalidator;
  protected GranularTimeCacheService $gtc;

  protected function setUp(): void {
    parent::setUp();
    $this->moduleRoot = dirname(__DIR__, 3);
    require_once("$this->moduleRoot/granulartimecache.module");

    $this->time = new FakeTime();
    $this->timeZone = new FakeTimeZone();
    $this->cacheTagsInvalidator = new CacheTagsInvalidatorSpy();
    $state = new State(new KeyValueMemoryFactory(), new NullBackend('null'), new NullLockBackend());
    $moduleHandlerProphecy = $this->prophesize(ModuleHandlerInterface::class);
    /** @noinspection PhpParamsInspection */
    $moduleHandlerProphecy->invokeAll('granulartimecache', Argument::type('array'))
      ->will(fn($args) => \granulartimecache_granulartimecache($args[1][0]));

    $moduleHandler = $moduleHandlerProphecy->reveal();
    $this->gtc = new GranularTimeCacheService(
      $this->time,
      $state,
      $this->cacheTagsInvalidator,
      $moduleHandler,
      $this->timeZone,
    );
  }


  public function testGTCCollection(): void {
    $this->assertNotEmpty($this->gtc);
    $getGTCCollection = $this->freeMethod($this->gtc, 'getTimeGranularityCollection');
    /** @var \Drupal\granulartimecache\Granularity\GranularityCollection $collection */
    $collection = $getGTCCollection();
    $this->assertNotEmpty($collection);
    $this->assertSame(['hourly', 'daily', 'monthly'], array_keys($collection->getGranularities()));
  }

  /**
   * @dataProvider provideTimeZone
   */
  public function testGTC(\DateTimeZone $timeZone): void {
    $this->timeZone->setTimeZone($timeZone);
    foreach (self::provideTime($timeZone) as [$time, $tags]) {
      assert($time instanceof \DateTime);
      $this->time->set($time);
      $this->gtc->cron();
      $this->assertSame($tags, $this->cacheTagsInvalidator->getTags(), $time->format(\DateTimeInterface::W3C));
    }
  }

  private static function provideTime(\DateTimeZone $timeZone) {
    yield [new \DateTime('2023-03-14 13:37:00', $timeZone), ['granulartimecache:hourly', 'granulartimecache:daily', 'granulartimecache:monthly']];
    yield [new \DateTime('2023-03-14 13:37:00', $timeZone), []];
    yield [new \DateTime('2023-03-14 13:38:00', $timeZone), []];
    yield [new \DateTime('2023-03-14 13:59:59', $timeZone), []];
    yield [new \DateTime('2023-03-14 14:00:00', $timeZone), ['granulartimecache:hourly']];
    yield [new \DateTime('2023-03-14 14:23:00', $timeZone), []];
    yield [new \DateTime('2023-03-14 15:23:00', $timeZone), ['granulartimecache:hourly']];
    yield [new \DateTime('2023-03-15 00:00:00', $timeZone), ['granulartimecache:hourly', 'granulartimecache:daily']];
    yield [new \DateTime('2023-03-31 00:00:00', $timeZone), ['granulartimecache:hourly', 'granulartimecache:daily']];
    yield [new \DateTime('2023-04-01 13:37:00', $timeZone), ['granulartimecache:hourly', 'granulartimecache:daily', 'granulartimecache:monthly']];
  }

  public static function provideTimeZone() {
    yield [new \DateTimeZone('UTC')];
    yield [new \DateTimeZone('Europe/Berlin')];
    yield [new \DateTimeZone('Pacific/Tongatapu')];
  }

  protected function freeMethod(object $object, string $methodName): \Closure {
    $classRef = new \ReflectionClass($object);
    $methodRef = $classRef->getMethod($methodName);
    $methodRef->setAccessible(true); // Needed below php 8.1.
    return function (...$args) use ($object, $methodRef) {
      return $methodRef->invokeArgs($object, $args);
    };
  }

}
