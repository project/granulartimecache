<?php

declare(strict_types=1);
namespace Drupal\Tests\granulartimecache\Unit\Double;

use Drupal\Component\Datetime\TimeInterface;

final class FakeTime implements TimeInterface {

  protected \DateTime $time;

  public function __construct() {
    $this->time = new \DateTime('1970-01-01 00:00:00');
  }

  public function set(\DateTime $time): void {
    $this->time = $time;
  }

  public function getRequestTime() {
    return $this->time->getTimestamp();
  }

  public function getRequestMicroTime() {
    return (float) $this->time->getTimestamp();
  }

  public function getCurrentTime() {
    return $this->time->getTimestamp();
  }

  public function getCurrentMicroTime() {
    return (float) $this->time->getTimestamp();
  }

}
