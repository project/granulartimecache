<?php

declare(strict_types=1);
namespace Drupal\Tests\granulartimecache\Unit\Double;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;

final class CacheTagsInvalidatorSpy implements CacheTagsInvalidatorInterface {

  protected array $tags;

  public function getTags(): array {
    return $this->tags;
  }

  public function invalidateTags(array $tags) {
    $this->tags = $tags;
  }

}
