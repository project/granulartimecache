<?php

declare(strict_types=1);
namespace Drupal\Tests\granulartimecache\Unit\Double;

use Drupal\granulartimecache\GranularTimeCacheTimeZoneInterface;

final class FakeTimeZone implements GranularTimeCacheTimeZoneInterface {

  protected \DateTimeZone $timeZone;

  public function __construct() {
    $this->timeZone = new \DateTimeZone('UTC');
  }

  public function getTimeZone(): \DateTimeZone {
    return $this->timeZone;
  }

  public function setTimeZone(\DateTimeZone $timeZone): void {
    $this->timeZone = $timeZone;
  }

}
