<?php

namespace Drupal\granulartimecache;

interface GranularTimeCacheTimeZoneInterface {

  public function getTimeZone(): \DateTimeZone;

}