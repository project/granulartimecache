<?php

namespace Drupal\granulartimecache;

/**
 * Helper to get locale offsets. Currently unused.
 *
 * @internal
 */
final class DateTimeOffsets {

  /**
   * Get start of day offset in seconds.
   *
   * Timezone is configured globally at admin/config/regional/settings, plus
   * optionally per user. TimeZoneResolver syncs this with PHP.
   *
   * @return int
   *
   * @see \Drupal\system\TimeZoneResolver::getTimeZone
   * @link https://www.php.net/manual/en/datetime.format.php
   */
  public static function startOfDay(): int {
    $timezoneOffsetInSeconds = date('Z');
    return intval($timezoneOffsetInSeconds);
  }

  /**
   * Get start of week offset in seconds.
   *
   * @return int
   *
   * @see \Drupal\system\Form\RegionalForm::buildForm
   */
  public static function startOfWeek(): int {
    // First day of week, 0 is sunday.
    $firstDayOfWeek = \Drupal::config('system.date')->get('first_day');
    // Unix timestamp 0 was thursday, so add 3.
    $oneDay = 60 * 60 * 24;
    return $oneDay * (3 + $firstDayOfWeek);
  }

}