<?php

declare(strict_types=1);
namespace Drupal\granulartimecache;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;

final class GranularTimeCacheTimeZone implements GranularTimeCacheTimeZoneInterface {

  protected ConfigFactoryInterface $configFactory;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  public function getTimeZone(): \DateTimeZone {
    $timezoneId = $this->getConfig()->get('timezone.default');
    return new \DateTimeZone($timezoneId);
  }

  protected function getConfig(): Config {
    return $this->configFactory->get('system.date');
  }
}
