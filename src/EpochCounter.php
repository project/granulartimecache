<?php

namespace Drupal\granulartimecache;

use Drupal\granulartimecache\Granularity\GranularityCollection;
use Drupal\granulartimecache\Granularity\GranularityCollectionBuilder;

/**
 * Counts invalidated epochs per granularity.
 *
 * An epoch is the time spanned by a granularity, e.g. Monday 07:00 till next.
 * We do it this way because of robustness: As DateTimeOffsets::startOfDay
 * will change e.g. with DST, this is more robust that saving lastInvalidated.
 */
class EpochCounter {

  /**
   * @var array<int, string>
   *   Maps TimeGranularity->name() => epochs.
   */
  protected array $invalidatedEpochMap;

  public function filterChangedEpoch(GranularityCollection $granularityCollection, \DateTime $dateTime): GranularityCollection {
    $granularityCollector = new GranularityCollectionBuilder();
    foreach ($granularityCollection->getGranularities() as $granularity) {
      $currentEpoch = $granularity->getEpoch($dateTime);
      $invalidatedEpoch = $this->invalidatedEpochMap[$granularity->getName()] ?? 0;
      if ($currentEpoch > $invalidatedEpoch) {
        $granularityCollector->add($granularity);
        $this->invalidatedEpochMap[$granularity->getName()] = $currentEpoch;
      }
    }
    return $granularityCollector->freeze();
  }

}