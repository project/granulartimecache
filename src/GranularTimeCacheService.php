<?php

namespace Drupal\granulartimecache;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\granulartimecache\Granularity\GranularityBase;
use Drupal\granulartimecache\Granularity\GranularityCollection;
use Drupal\granulartimecache\Granularity\GranularityCollectionBuilder;

class GranularTimeCacheService {

  protected TimeInterface $timeService;

  protected StateInterface $state;

  protected CacheTagsInvalidatorInterface $cacheTagsInvalidator;

  protected ModuleHandlerInterface $moduleHandler;

  protected GranularTimeCacheTimeZoneInterface $timezone;

  public function __construct(TimeInterface $timeService, StateInterface $state, CacheTagsInvalidatorInterface $cacheTagsInvalidator, ModuleHandlerInterface $moduleHandler, GranularTimeCacheTimeZoneInterface $timezone) {
    $this->timeService = $timeService;
    $this->state = $state;
    $this->cacheTagsInvalidator = $cacheTagsInvalidator;
    $this->moduleHandler = $moduleHandler;
    $this->timezone = $timezone;
  }

  public function cron(): void {
    $epochCounter = $this->loadEpochCounter();
    $granularityCollection = $this->getTimeGranularityCollection();
    $timestamp = $this->timeService->getRequestTime();
    $dateTime = (new \DateTime("@$timestamp"))->setTimezone($this->timezone->getTimeZone());
    $granularitiesToInvalidate = $epochCounter->filterChangedEpoch($granularityCollection, $dateTime);

    $cacheTagsToInvalidate = array_values(array_map(
      fn(GranularityBase $granularity) => $this->makeCacheTag($granularity),
      $granularitiesToInvalidate->getGranularities()
    ));
    $this->cacheTagsInvalidator->invalidateTags($cacheTagsToInvalidate);

    $this->saveEpochCounter($epochCounter);
  }

  protected function getTimeGranularityCollection(): GranularityCollection {
    $collector = new GranularityCollectionBuilder();
    $this->moduleHandler->invokeAll('granulartimecache', [$collector]);
    return $collector->freeze();
  }

  protected function makeCacheTag(GranularityBase $granularity): string {
    return "granulartimecache:{$granularity->getName()}";
  }

  protected function loadEpochCounter(): EpochCounter {
    return $this->state->get('granulartimecache_epoch_counter')
      ?? new EpochCounter();
  }

  protected function saveEpochCounter(EpochCounter $epochCounter): void {
    $this->state->set('granulartimecache_epoch_counter', $epochCounter);
  }

}
