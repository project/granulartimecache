<?php

namespace Drupal\granulartimecache;


/**
 * Time granularity value object.
 */
interface GranularityInterface {

  public function getName(): string;

  /**
   * Get epoch.
   *
   * @param \DateTime $timestamp
   *   The current timestamp.
   *
   * @return string
   *   An arbitrary string describing the epoch of the current granularity.
   *   Example: "2010-10-10" may be an epoch of "daily".
   */
  public function getEpoch(\DateTime $dateTime): string;

}