<?php

namespace Drupal\granulartimecache\Granularity;

use Drupal\granulartimecache\GranularityInterface;

abstract class GranularityBase implements GranularityInterface {

  protected string $name;

  public function __construct(string $name) {
    $this->name = $name;
  }

  public function getName(): string {
    return $this->name;
  }
  
  abstract public function getEpoch(\DateTime $dateTime): string;

}