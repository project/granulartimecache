<?php

namespace Drupal\granulartimecache\Granularity;

/**
 * Time granularity collection
 */
class GranularityCollectionBuilder {

  /**
   * @var array<\Drupal\granulartimecache\Granularity\GranularityBase>
   */
  protected array $timeGranularityMap = [];

  public function freeze(): GranularityCollection {
    return new GranularityCollection($this->timeGranularityMap);
  }

  public function add(GranularityBase $granularity) {
    // Yell if name conflict, but be graceful if equal granularity re-added.
    if ($existing = $this->timeGranularityMap[$granularity->getName()] ?? NULL) {
      /** @noinspection PhpNonStrictObjectEqualityInspection */
      if ($existing == $granularity) {
        return;
      }
      else {
        throw new \UnexpectedValueException("Conflicting declaration of granularity '{$granularity->getName()}'.");
      }
    }
    $this->timeGranularityMap[$granularity->getName()] = $granularity;
  }

}