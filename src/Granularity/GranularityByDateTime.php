<?php

namespace Drupal\granulartimecache\Granularity;

class GranularityByDateTime extends GranularityBase {

  protected string $dateTimeFormat;

  public function __construct(string $name, string $dateTimeFormat) {
    parent::__construct($name);
    $this->dateTimeFormat = $dateTimeFormat;
  }


  public function getEpoch(\DateTime $dateTime): string {
    return $dateTime->format($this->dateTimeFormat);
  }

}