<?php

namespace Drupal\granulartimecache\Granularity;

/**
 * Exact granularity.
 *
 * Helps to do cache tag invalidation exactly hourly.
 */
class GranularityByTimestamp extends GranularityBase {

  protected int $duration;

  protected int $offset;

  public function __construct(string $name, int $duration, int $offset = 0) {
    parent::__construct($name);
    $this->duration = $duration;
    $this->offset = $offset;
  }

  public function getEpoch(\DateTime $dateTime): string {
    return strval(intdiv($dateTime->getTimestamp() - $this->offset, $this->duration));
  }

}