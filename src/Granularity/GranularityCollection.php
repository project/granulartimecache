<?php

namespace Drupal\granulartimecache\Granularity;

/**
 * Time granularity collection
 */
class GranularityCollection {

  /**
   * @var array<\Drupal\granulartimecache\Granularity\GranularityBase>
   */
  protected array $timeGranularityMap;

  /**
   * @param \Drupal\granulartimecache\Granularity\GranularityBase[] $timeGranularityMap
   */
  public function __construct(array $timeGranularityMap) {
    $this->timeGranularityMap = $timeGranularityMap;
  }

  /**
   * @return array<\Drupal\granulartimecache\Granularity\GranularityBase>
   */
  public function getGranularities(): array {
    return $this->timeGranularityMap;
  }

  public function filter(\Closure $closure): self {
    return new self(array_filter($this->timeGranularityMap, $closure));
  }

}