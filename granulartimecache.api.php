<?php

/*
 * Implements hook_granulartimecache.
 *
 * Every granularity defined here creates a corresponding cache tag.
 * In this case: 'granulartimecache:daily_at_6'.
 * All offsets are based on start of week in current timezone.
 *
 * @see \Drupal\granulartimecache\DateTimeOffsets
 */

use Drupal\granulartimecache\Granularity\GranularityByDateTime;

function hook_granulartimecache(\Drupal\granulartimecache\Granularity\GranularityCollectionBuilder $collector) {
  $collector->add(new GranularityByDateTime('calendar_weekly', 'Y-W'));
}
