# Granular Time Cache
Provides cache tags that invalidate with time.

## Example use case
Think of an event calendar that only shows events whose date is in the future. How to cache that view?
You can make the view uncacheable, and and event will disappear the exact second it is finished.
But then you have an uncacheable page, very bad for performance.
You can set view granularity to daily. Then all "today and future" events are displayed, which is usually fine.
Give it a cache max-age and... no that will not work out.
What you need is a cache tag that invalidates exactly at midnight.

This module to the rescue. It provides a "granulartimecache:daily" cache tag.
Use the file [Views Custom Cache Tags](https://www.drupal.org/project/views_custom_cache_tag) module, and you've won.

## Available cache tags
The module defines the following granularity cache tags:
- hourly
- daily
- monthly
- hourly_exact

The last one is for educational purpose: 'hourly_exact' will always last for 60 minutes, while daily will last for 25 h on some days (think DST).

You can implement a hook to add additional tags. If you are convinced that any additional granularity is of popular demand, create an issue, and for best karma points, provide a MR.

## Limitations
For the regular calendar-based cache tags, this module supports only one time zone: That set in `admin/config/regional/settings` (or that of the cron user if timezone by user is set).

For who needs it: Implementing cache tags per timezone should be straightforward though. MR appreciated.
